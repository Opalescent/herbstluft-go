## Herbstluft Go

Just a small project to configure the Herbstluft window manager using Go instead of the default, Bash.

## Motivation

I really did not enjoy configuring herbstluft in Bash. It's wasn't pretty and it was getting difficult to keep things organized.

## Installation

For the time being, since this is only a personal project at the moment, the configuration is managed in a source code file. Therefore if you do decide to `go get`, it then be aware that you'll need to re-build every time you change. After building I just symlink the resulting binary to ~/.config/autostart.

I'm currently in the process of moving the configuration to an external file, most likely TOML, and adding a parser.

## Credits 

This project was heavily inspired by [this blog]. I didn't see Go on his list and I thought it would be an interesting project.

<!-- Links -->

[this blog]: http://epsi-rns.github.io/desktop/2017/05/01/herbstlustwm-modularized-overview.html
