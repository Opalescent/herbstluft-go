package main

import (
    "os"

    "gitlab.com/Opalescent/hl"
)

func main() {
    if hl.IsLocked() {
        // If the autostart is already running do not
        // run it again until herbstclient is unlocked.
        os.Exit(1)
    }

    check_can_reload() // utils.go

    hl.Run("attr", "my_can_reload", "true")

    hl.Run("emit_hook", "reload")

    hl.Run("keyunbind", "--all")
    hl.Run("mouseunbind", "--all")
    hl.Run("unrule", "-F")

    setup_tags() // utils.go

    hl.Lock() // Locking here since nothing gets repainted until the configs get changed

    setup_config() // utils.go

    startup() // utils.go

    hl.Unlock()

    hl.Run("attr", "my_can_reload", "true")
}
