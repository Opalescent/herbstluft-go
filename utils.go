package main

import (
    "fmt"
    "os"
    "os/exec"

    "gitlab.com/Opalescent/hl"
)

func check_can_reload() {
    can_reload, _ := exec.Command("herbstclient", "attr", "my_can_reload").Output()

    if (string(can_reload) == "false") {
        os.Exit(1)
    } else if (len(can_reload) == 0) {
        hl.Run("new_attr", "bool", "my_can_reload", "false")
    }
}

func setup_tags() {
    hl.Run("rename", "default", Tags[0])

    var tag_keybinds []Keybind

    for index, tag := range Tags {
        hl.Run("add", tag)

        tag_keybinds = append(tag_keybinds, []Keybind{
            { key: fmt.Sprintf("%s-%d", Mod, index+1),       command: "use",  args: []string{tag} },
            { key: fmt.Sprintf("%s-Shift-%d", Mod, index+1), command: "move", args: []string{tag} },
        }...)
    }

    Keybinds = append(Keybinds, tag_keybinds...)
}

func setup_config() {
    for _, keybind := range Keybinds {
        hl.Run(append([]string{"keybind", keybind.key, keybind.command}, keybind.args...)...)
    }

    for _, mousebind := range Mousebinds {
        hl.Run(append([]string{"mousebind", mousebind.button, mousebind.action}, mousebind.command...)...)
    }

    for _, attribute := range Attributes {
        hl.Run([]string{"attr", attribute.path, attribute.value}...)
    }

    for _, setting := range Settings {
        hl.Run([]string{"set", setting.path, setting.value}...)
    }

    for _, rule := range Rules {
        var args []string

        for _, arg := range [][]string{rule.labels, rule.flags, rule.conditions, rule.consequences} {
            args = append(args, arg...)
        }

        hl.Run(append([]string{"rule"}, args...)...)
    }
}

func startup() {
    should_startup, _ := exec.Command("herbstclient", "attr", "my_autostarted").Output()

    if (len(should_startup) == 0) {
        exec.Command("feh", "--bg-fill", "--randomize", "/home/lewd/Pictures/Wallpapers").Run()
        exec.Command("compton").Start()

        hl.Run("new_attr", "bool", "my_autostarted")
    }
}
