package main

var Colors = map[string]string{
    "black":         "#021b21",
    "light_black":   "#234149",

    "red":           "#c2454e",
    "light_red":     "#ef5847",

    "green":         "#7cbf9e",
    "light_green":   "#a2d9b1",

    "yellow":        "#8a7a63",
    "light_yellow":  "#beb090",

    "blue":          "#2e3340",
    "light_blue":    "#61778d",

    "magenta":       "#ff5879",
    "light_magenta": "#ff99a1",

    "cyan":          "#44b5b1",
    "light_cyan":    "#9ed9d8",

    "white":         "#f2f1b9",
    "light_white":   "#f6f6c9",
}
