package main

type Keybind struct {
    key     string
    command string
    args    []string
}

type Mousebind struct {
    button  string
    action  string
    command []string
}

type Attribute struct {
    path  string
    value string
}

type Setting struct {
    path  string
    value string
}

type Rule struct {
    flags        []string
    labels       []string
    conditions   []string
    consequences []string
}
