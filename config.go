package main

const Mod string = "Mod4"

var Tags = []string{"web", "term", "dev", "chat", "music"}

var Keybinds = []Keybind{
    // Quitting, reloading, and closing
    { key: Mod+"-Shift-q",       command: "quit"                                             },
    { key: Mod+"-Shift-r",       command: "reload"                                           },
    { key: Mod+"-Shift-c",       command: "close"                                            },

    // Spawning
    // { key: Mod+"-Return",        command: "spawn",      args: []string{"${TERMINAL:-urxvt}"} },
    { key: Mod+"-Return",        command: "spawn",      args: []string{"urxvt"}              },
    { key: Mod+"-Shift-Return",  command: "spawn",      args: []string{"firefox"}            },

    // Focusing clients
    { key: Mod+"-h",             command: "focus",      args: []string{"left"}               },
    { key: Mod+"-j",             command: "focus",      args: []string{"down"}               },
    { key: Mod+"-k",             command: "focus",      args: []string{"up"}                 },
    { key: Mod+"-l",             command: "focus",      args: []string{"right"}              },

    // Moving clients
    { key: Mod+"-Shift-h",       command: "shift",      args: []string{"left"}               },
    { key: Mod+"-Shift-j",       command: "shift",      args: []string{"down"}               },
    { key: Mod+"-Shift-k",       command: "shift",      args: []string{"up"}                 },
    { key: Mod+"-Shift-l",       command: "shift",      args: []string{"right"}              },

    // Splitting frames
    { key: Mod+"-u",             command: "split",      args: []string{"bottom", "0.5"}      },
    { key: Mod+"-o",             command: "split",      args: []string{"right",  "0.5"}      },
    { key: Mod+"-Control-space", command: "split",      args: []string{"explode"}            },

    // Resizing frames
    { key: Mod+"-Control-h",     command: "resize",     args: []string{"left",  "0.05"}      },
    { key: Mod+"-Control-j",     command: "resize",     args: []string{"down",  "0.05"}      },
    { key: Mod+"-Control-k",     command: "resize",     args: []string{"up",    "0.05"}      },
    { key: Mod+"-Control-l",     command: "resize",     args: []string{"right", "0.05"}      },

    // Layouts
    { key: Mod+"-r",             command: "remove"                                           },
    { key: Mod+"-s",             command: "floating",   args: []string{"toggle"}             },
    { key: Mod+"-f",             command: "fullscreen", args: []string{"toggle"}             },
    { key: Mod+"-p",             command: "pseudotile", args: []string{"toggle"}             },
    { key: Mod+"-space",         command: "or",         args: []string{
        ",", "and", ".", "compare", "tags.focus.curframe_wcount = 2",
        ".", "cycle_layout", "+1", "vertical", "horizontal", "max", "vertical", "grid",
        ",", "cycle_layout", "+1"}                                                           },

    // More focus
    { key: Mod+"-BackSpace",     command: "cycle_monitor"                                    },
    { key: Mod+"-Tab",           command: "cycle_all",  args: []string{"+1"}                 },
    { key: Mod+"-Shift-Tab",     command: "cycle_all",  args: []string{"-1"}                 },
    { key: Mod+"-c",             command: "cycle"                                            },
    { key: Mod+"-i",             command: "jumpto",     args: []string{"urgent"}             },
}

var Mousebinds = []Mousebind{
    { button: Mod+"-Button1", action: "move"   },
    { button: Mod+"-Button2", action: "zoom"   },
    { button: Mod+"-Button3", action: "resize" },
}

var Attributes = []Attribute{
    { path: "theme.floating.reset",               value: "1"                     },
    { path: "theme.tiling.reset",                 value: "1"                     },

    //{ path: "theme.active.color",                 value: Colors["red"]           },
    { path: "theme.active.color",                 value: Colors["green"]         },
    { path: "theme.active.inner_color",           value: "#3e4a00"               },
    { path: "theme.active.outer_color",           value: Colors["green"]         },
    { path: "theme.active.border_width",          value: "2"                     },

    { path: "theme.normal.color",                 value: Colors["light_yellow"]  },
    { path: "theme.normal.inner_width",           value: "0"                     },
    { path: "theme.normal.outer_width",           value: "0"                     },
    { path: "theme.normal.border_width",          value: "2"                     },

    { path: "theme.floating.active.color",        value: Colors["green"]         },
    { path: "theme.floating.border_width",        value: "0"                     },
    { path: "theme.floating.active.border_width", value: "2"                     },
    { path: "theme.floating.outer_width",         value: "0"                     },

    { path: "theme.urgent.color",                 value: Colors["light_magenta"] },

    { path: "theme.background_color",             value: "#141414"       },
}

var Settings = []Setting{
    { path: "always_show_frame",         value: "1"          },
    { path: "mouse_recenter_gap",        value: "0"          },
    { path: "smart_frame_surroundings",  value: "1"          },
    { path: "smart_window_surroundings", value: "0"          },
    { path: "window_gap",                value: "0"          },

    { path: "frame_bg_normal_color",     value: "#565656"    },
    { path: "frame_bg_transparent",      value: "0"          },
    { path: "frame_active_opacity",      value: "0"          },
    { path: "frame_normal_opacity",      value: "50"         },
    { path: "frame_border_width",        value: "0"          },
    { path: "frame_gap",                 value: "10"         },
    { path: "frame_padding",             value: "-1"         },
    { path: "frame_transparent_width",   value: "0"          },

    { path: "tree_style",                value: "╾│ ├└╼─┐" },
}

var Rules = []Rule{
    { consequences: []string{"focus=on"} },
    {
        conditions: []string{"windowtype~'_NET_WM_WINDOW_TYPE_(DIALOG|UTILITY|SPLASH)'"},
        consequences: []string{"pseudotile=on"},
    },
    {
        conditions: []string{"windowtype='_NET_WM_WINDOW_TYPE_DIALOG'"},
        consequences: []string{"focus=on"},
    },
    {
        conditions: []string{"windowtype~'_NET_WM_WINDOW_TYPE_(NOTIFICATION|DOCK|DESKTOP)'"},
        consequences: []string{"manage=off"},
    },
}
